"""
Parâmetros a serem utilizados pelos diferentes
métodos para limiarização de imagens.
"""

p_global = {
    'T': 128
}

p_bernsen = {
    'n': 15
}

p_niblack = {
    'n': 15,
    'k': -0.5
}

p_sauvola = {
    'n': 9,
    'k': 0.1,
    'R': 128
}

p_phansalskar = {
    'n': 7,
    'k': 0.22,
    'R': 128,
    'p': 3,
    'q': 0.05
}

p_contraste = {
    'n': 15
}

p_media = {
    'n': 15
}

p_mediana = {
    'n': 15
}