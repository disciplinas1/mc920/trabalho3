"""
Módulo para limiarização de imagens em
escala de cinza utilizando diversas técnicas.
"""
from sys import version_info

if version_info.major < 3 or version_info.minor < 5:
    erro = 'Esse programa suporta versões de Python à partir da 3.5'
    raise RuntimeError(erro)

import numpy as np
import cv2
import argparse
from scipy.ndimage import generic_filter

import param


def le_imagem_cinza(arquivo: str):
    """
    Retorna uma imagem em escala de cinza (array 2D) com
    valores do tipo float referente ao arquivo especificado.
    """
    return cv2.imread(arquivo, flags=cv2.IMREAD_GRAYSCALE)


def trunca_e_salva(imagem, arquivo):
    """
    Trunca os valores da imagem para (0, 255) e salva.
    """
    a, b = 0, 255
    imagem[imagem < a] = a
    imagem[imagem > b] = b

    cv2.imwrite(arquivo, imagem)


def limiar_global(imagem, T):
    """
    Faz a limiarização global da imagem.
    """
    img = np.zeros(imagem.shape)

    img[imagem > T] = 255

    return img


def limiar_bernsen(imagem, block_size):
    """
    Faz a limiarização pelo método de Bernsen.
    """
    filtro = generic_filter(imagem, lambda x: (np.max(x) + np.min(x)) / 2, size=block_size)

    saida = np.zeros(imagem.shape)

    saida[imagem > filtro] = 255

    return saida


def limiar_niblack(imagem, block_size, k):
    """
    Faz a limiarização local com o método de Niblack,
    de acordo com o valor de 'k' e numa vizinhança quadrada
    de 'block_size' píxels de lado.
    """
    saida = cv2.ximgproc.niBlackThreshold(imagem, 255, cv2.THRESH_BINARY, block_size, k)

    return saida


def limiar_sauvola(imagem, block_size, k, R):
    """
    Faz a limiarização com o método de Sauvola, com os parâmetros
    'k' e 'R' numa vizinhança quadrada de 'block_size' píxels de lado.
    """
    saida = cv2.ximgproc.niBlackThreshold(
        imagem, 255, cv2.THRESH_BINARY, block_size, k,
        binarizationMethod=cv2.ximgproc.BINARIZATION_SAUVOLA,
        r=R
        )

    return saida


def limiar_phansalskar(imagem, block_size, k, R, p, q):
    """
    Faz a limiarização pelo método de Phansalkar.
    """
    def T(v):
        u = np.mean(v)
        s = np.std(v)

        return u * (1 + p * np.exp(-q * u) + k * ((s / R) - 1))

    filtro = generic_filter(imagem, T, size=block_size)

    saida = np.zeros(imagem.shape)

    saida[imagem > filtro] = 255

    return saida


def limiar_contraste(imagem, block_size):
    """
    Faz a limiarização pelo método do contraste.
    """
    return 255 - limiar_bernsen(imagem, block_size)


def limiar_media(imagem, block_size):
    """
    Faz a limiarização pelo método da média.
    """
    filtro = generic_filter(imagem, np.mean, size=block_size)

    saida = np.zeros(imagem.shape)

    saida[imagem > filtro] = 255

    return saida


def limiar_mediana(imagem, block_size):
    """
    Faz a limiarização pelo método da mediana.
    """
    filtro = generic_filter(imagem, np.median, size=block_size)

    saida = np.zeros(imagem.shape)

    saida[imagem > filtro] = 255

    return saida


def main():
    """
    Lê os argumentos utilizados e realiza a limiarização.
    Salva a imagem de saída de acordo com o nome especificado.
    """
    parser = argparse.ArgumentParser(description='Limiariza imagens com diferentes técnicas.')

    parser.add_argument('IMAGEM', action='store', nargs=1, type=str,
        help='arquivo da imagem que será processada'
    )
    
    parser.add_argument('SAIDA', action='store', nargs=1, type=str,
        help='nome do arquivo de saída'
    )

    parser.add_argument('METODO', action='store', nargs=1, type=str,
        help='metodo de limiarização a ser utilizado'
    )

    # Lê os argumentos utilizados.
    if version_info.minor < 7:
        args = parser.parse_args()
    else:
        args = parser.parse_intermixed_args()


    img = le_imagem_cinza(args.IMAGEM[0])
    if img is None:
        raise FileNotFoundError


    metodo = args.METODO[0]

    args_dict = getattr(param, 'p_' + metodo)

    saida = globals()['limiar_' + metodo](img, *args_dict.values())

    trunca_e_salva(saida, args.SAIDA[0])


if __name__ == '__main__':
    main()
